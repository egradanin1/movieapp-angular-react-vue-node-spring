import Vue from 'vue';
import Router from 'vue-router';
import Login from '@/components/Login';
import Register from '@/components/Register';
import Dashboard from '@/components/Dashboard';
import MyMovies from '@/components/MyMovies';
import MovieInfo from '@/components/MovieInfo';
import Chat from '@/components/Chat';
import Adminboard from '@/components/Adminboard';
import Benchmark from '@/components/Benchmark';
import BenchmarkResults from '@/components/BenchmarkResults';

Vue.use(Router);

export default new Router({
	routes: [
		{
			path: '/',
			name: 'dashboard',
			component: Dashboard
		},
		{
			path: '/login',
			name: 'login',
			component: Login
		},
		{
			path: '/register',
			name: 'register',
			component: Register
		},
		{
			path: '/mymovies',
			name: 'mymovie',
			component: MyMovies
		},
		{
			path: '/details/:title',
			name: 'details',
			component: MovieInfo
		},
		{
			path: '/chat',
			name: 'chat',
			component: Chat
		},
		{
			path: '/adminboard',
			name: 'adminboard',
			component: Adminboard
		},
		{
			path: '/benchmark',
			name: 'benchmark',
			component: Benchmark
		},
		{
			path: '/benchmarkresults',
			name: 'benchmarkresults',
			component: BenchmarkResults
		}
	]
});
