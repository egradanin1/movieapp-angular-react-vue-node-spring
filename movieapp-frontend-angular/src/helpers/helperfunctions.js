let Options = (function() {
	var generateRandom = max => {
		return Math.round(Math.random() * 1000) % max;
	};
	var genres = [
		'Adventure',
		'Action',
		'Comedy',
		'Crime',
		'Drama',
		'Fantasy',
		'Historical',
		'Horror',
		'Mystery',
		'Philosophical',
		'Political',
		'Romance',
		'Saga',
		'Satire',
		'Science fiction',
		'Speculative',
		'Thriller'
	];
	var titles = [
		'The Seven Samurai',
		'Bonnie and Clyde',
		'Reservoir Dogs',
		'Airplane!',
		"Pan's Labyrinth",
		'Doctor Zhivago',
		'The Deer Hunter',
		'Close Encounters of the Third Kind',
		'Up',
		'Rocky',
		'Memento',
		'Braveheart',
		'Slumdog Millionaire',
		'The Lord of the Rings: The Return of the King',
		'Beauty and the Beast',
		'Seven',
		'Inception',
		'Die Hard',
		'The Lord of the Rings: The Fellowship of the Ring',
		'Amadeus',
		'On the Waterfront',
		'Wall-E',
		'12 Angry Men',
		'Ghostbusters',
		'Brokeback Mountain',
		'The Bridge on the River Kwai',
		'Blazing Saddles',
		"All the President's Men",
		'Young Frankenstein',
		'Almost Famous',
		'Vertigo',
		'Gladiator',
		'Monty Python and The Holy Grail',
		'Avatar',
		'The Lion King',
		'Raging Bull',
		'Mary Poppins',
		'Groundhog Day',
		'North by Northwest',
		'West Side Story',
		'Amelie',
		'Thelma & Louise',
		'Sunset Blvd.',
		'The Dark Knight',
		'Eternal Sunshine of the Spotless Mind',
		'Taxi Driver',
		'Butch Cassidy and the Sundance Kid',
		'Good Will Hunting',
		'All About Eve',
		'The Big Lebowski'
	];
	var actors = [
		'Chris Evans',
		'Robert Downey, Jr.',
		'Jennifer Lawrence',
		'Channing Tatum',
		'George Clooney',
		'Johnny Depp',
		'Margot Robbie',
		'Jared Leto',
		'Ryan Reynolds',
		'Ryan Gosling',
		'Scarlett Johansson',
		'Brad Pitt',
		'Matt Damon',
		'Will Smith',
		'Paul Rudd',
		'Ben Affleck',
		'Matthew Mcconaughey',
		'Tom Hardy',
		'Chris Hemsworth',
		'Dwayne Johnson',
		'Tom Hiddleston',
		'Jamie Foxx',
		'Emma Stone',
		'Nicolas Cage',
		'Tom Cruise',
		'Chris Pratt',
		'Samuel L. Jackson',
		'Leonardo Dicaprio',
		'Tom Holland',
		'Tyler Perry',
		'Gal Gadot',
		'Idris Elba',
		'Tom Hanks',
		'Cara Delevingne',
		'Will Ferrell',
		'Ben Stiller',
		'Melissa Mccarthy',
		'Vin Diesel',
		'Charlize Theron',
		'Anna Kendrick',
		'Bradley Cooper',
		'Amy Poehler',
		'Mark Wahlberg',
		'Jonah Hill',
		'Daisy Ridley',
		'Jessica Chastain',
		'Kristen Wiig',
		'James Mcavoy',
		'Denzel Washington',
		'Felicity Jones'
	];
	var data = [];
	return {
		makeData: numberOfRows => {
			Array(numberOfRows)
				.fill(1)
				.map(el =>
					data.push({
						genre: genres[generateRandom(genres.length)],
						title: titles[generateRandom(titles.length)],
						actor: actors[generateRandom(actors.length)]
					})
				);
			return data;
		},
		updateData: () =>
			(data = data.map(
				(el, index) =>
					index % 5 === 0
						? Object.assign({}, el, {
								title: el.title
									.split('')
									.reverse()
									.join('')
						  })
						: el
			)),
		deleteData: () => (data = []),
		deleteRow: index => (data = data.filter((el, ind) => index !== ind))
	};
})();

export default Options;
