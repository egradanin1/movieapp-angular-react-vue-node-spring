import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import Errors from '../../../helpers/Errors';
import { Validator, Handler, ErrorMessages } from '../../../helpers/Validator';
import axios from 'axios';

@Component({
	selector: 'app-register',
	templateUrl: './register.component.html',
	styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
	username = '';
	password = '';
	firstname = '';
	lastname = '';
	email = '';
	confpassword = '';
	errors = new Errors();

	constructor(private router: Router) {}

	ngOnInit() {}

	handleInput(e) {
		if (!Handler[e.target.name](e.target.value, this.password)) {
			this.errors.record({
				[e.target.name]: ErrorMessages[e.target.name]
			});
			this[e.target.name] = e.target.value;
		} else {
			this.errors.clear(e.target.name);
			this[e.target.name] = e.target.value;
		}
	}
	handleClick() {
		axios
			.post('/api/registration', {
				username: this.username,
				password: this.password,
				firstName: this.firstname,
				lastName: this.lastname,
				email: this.email
			})
			.then(response => this.router.navigate(['/dashboard']))
			.catch(error => console.log(error));
	}

	areInputsEmpty() {
		return !['username', 'password'].every(key => this[key].length !== 0);
	}
}
