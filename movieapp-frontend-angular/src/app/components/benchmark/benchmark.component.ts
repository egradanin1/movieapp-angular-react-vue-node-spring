import { Component, OnInit, NgModule, AfterViewChecked } from '@angular/core';
import { AdminheaderComponent } from '../adminheader/adminheader.component';
import Options from '../../../helpers/helperfunctions';
import axios from 'axios';

var startTime;
var endTime;
var actionName;
var duration;

@Component({
	selector: 'app-benchmark',
	templateUrl: './benchmark.component.html',
	styleUrls: ['./benchmark.component.css']
})
@NgModule({
	declarations: [BenchmarkComponent, AdminheaderComponent],
	imports: [AdminheaderComponent],
	exports: [BenchmarkComponent],
	providers: []
})
export class BenchmarkComponent implements AfterViewChecked {
	shows = [];
	constructor() {}

	ngOnInit() {}

	ngAfterViewChecked() {
		if (actionName) this.calculateDuration();
	}

	calculateDuration() {
		window.setTimeout(() => {
			endTime = performance.now();
			duration = endTime - startTime;
			axios
				.post('/api/benchmark', {
					action: actionName,
					duration: duration,
					framework: 'angular'
				})
				.then(response => console.log(response))
				.catch(error => console.log(error));
			actionName = '';
		}, 0);
	}
	measureTime(fun, name) {
		actionName = name;
		startTime = performance.now();
		fun.bind(this)();
	}
	deleteShow(index) {
		this.shows = Options.deleteRow(index);
	}
	createShows() {
		if (!this.shows.length) {
			this.shows = Options.makeData(500);
		}
	}
	addShows() {
		this.shows = Options.makeData(500);
	}
	updateShows() {
		this.shows = Options.updateData();
	}
	deleteShows() {
		this.shows = Options.deleteData();
	}
}
