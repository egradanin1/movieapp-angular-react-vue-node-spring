import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BenchmarkresultsComponent } from './benchmarkresults.component';

describe('BenchmarkresultsComponent', () => {
  let component: BenchmarkresultsComponent;
  let fixture: ComponentFixture<BenchmarkresultsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BenchmarkresultsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BenchmarkresultsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
