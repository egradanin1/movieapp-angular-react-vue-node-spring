import { Component, OnInit, NgModule } from '@angular/core';
import { AdminheaderComponent } from '../adminheader/adminheader.component';
import axios from 'axios';

@Component({
	selector: 'app-benchmarkresults',
	templateUrl: './benchmarkresults.component.html',
	styleUrls: ['./benchmarkresults.component.css']
})
@NgModule({
	declarations: [BenchmarkresultsComponent, AdminheaderComponent],
	imports: [AdminheaderComponent],
	exports: [BenchmarkresultsComponent],
	providers: []
})
export class BenchmarkresultsComponent implements OnInit {
	benchmarks = [];
	filters = {};
	averageFrameworkScores = '';
	filteredBenchmarks = [];

	constructor() {}

	ngOnInit() {
		this.getBenchmarks();
	}
	handleInput(e) {
		if (e.target.value) {
			if (this.filters && this.filters.hasOwnProperty(e.target.name)) delete this.filters[e.target.name];
			this.filters = Object.assign({ [e.target.name]: e.target.value }, this.filters);
		} else {
			let clone = Object.assign({}, this.filters);
			delete clone[e.target.name];
			this.filters = clone;
		}
		this.filteredBenchmarks = this.benchmarks.filter(benchmark =>
			Array.from(Object.keys(this.filters)).every(
				f =>
					f !== 'duration'
						? Boolean(~benchmark[f].toLowerCase().indexOf(this.filters[f].toLowerCase()))
						: Number(benchmark[f]) < Number(this.filters[f])
			)
		);
	}
	averageScores(framework) {
		let averageFrameworkScores = '';
		let groupedBenchmarksByAction;
		Array.from(
			Object.keys(
				(groupedBenchmarksByAction = this.benchmarks
					.filter(el => el.framework === framework)
					.reduce((acc, el) => {
						acc[el.action] = acc[el.action] || [];
						acc[el.action].push(el);
						return acc;
					}, Object.create(null)))
			)
		).map(
			el =>
				(averageFrameworkScores += `${el} ---> ${(
					groupedBenchmarksByAction[el].reduce((acc, score) => acc + Number(score.duration), 0) /
					groupedBenchmarksByAction[el].length
				).toFixed(2)} ms <br/>`)
		);
		this.averageFrameworkScores = averageFrameworkScores;
		document.getElementById('averageScore-container').innerHTML = this.averageFrameworkScores;
	}
	getBenchmarks() {
		axios
			.get('/api/benchmarks')
			.then(response => {
				this.benchmarks = Object.freeze(response.data);
				this.filteredBenchmarks = response.data;
			})
			.catch(error => console.log(error));
	}
}
