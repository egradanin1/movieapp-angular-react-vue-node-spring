import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import Errors from '../../../helpers/Errors';
import { Validator, Handler, ErrorMessages } from '../../../helpers/Validator';
import axios from 'axios';

@Component({
	selector: 'app-login',
	templateUrl: './login.component.html',
	styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
	username = '';
	password = '';
	errors = new Errors();
	constructor(private router: Router) {}

	ngOnInit() {}
	handleInput(e) {
		if (!Handler[e.target.name](e.target.value)) {
			this.errors.record({
				[e.target.name]: ErrorMessages[e.target.name]
			});
			this[e.target.name] = e.target.value;
		} else {
			this.errors.clear(e.target.name);
			this[e.target.name] = e.target.value;
		}
	}
	handleClick() {
		axios
			.post('/api/login', {
				username: this.username,
				password: this.password
			})
			.then(response => {
				if (response.data) {
					response.data.role === 'admin'
						? this.router.navigate(['/adminboard'])
						: this.router.navigate(['/dashboard']);
				} else {
					alert('Bad credentials!');
				}
			})
			.catch(error => console.log(error));
	}

	areInputsEmpty() {
		return !['username', 'password'].every(key => this[key].length !== 0);
	}
}
