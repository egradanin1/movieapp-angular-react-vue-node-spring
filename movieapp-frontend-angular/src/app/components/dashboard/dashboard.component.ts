import { Component, OnInit, NgModule } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { HttpClientModule } from '@angular/common/http';
import { MoviecardComponent } from '../moviecard/moviecard.component';
import { HeaderComponent } from '../header/header.component';
import { Router } from '@angular/router';
import axios from 'axios';
import { Movie } from '../../shared/movie';

@Component({
	selector: 'app-dashboard',
	templateUrl: './dashboard.component.html',
	styleUrls: ['./dashboard.component.css']
})
@NgModule({
	declarations: [DashboardComponent, MoviecardComponent, HeaderComponent],
	imports: [MoviecardComponent, HeaderComponent, HttpClientModule],
	exports: [DashboardComponent],
	providers: []
})
export class DashboardComponent implements OnInit {
	movies = [];
	filteredMovies = [];
	user = null;
	filter = '';
	constructor(private router: Router, private http: HttpClient) {}

	ngOnInit() {
		this.getMovies();
	}

	handleInput(e) {
		this.filter = e.target.value;
		this.filteredMovies = this.movies.filter(
			movie => ~movie.title.toLowerCase().indexOf(this.filter.toLowerCase())
		);
	}
	getMovies() {
		this.http.get('/api/loggeduser', { responseType: 'text' }).subscribe(auth => {
			auth != 'anonymousUser' && auth != 'Admin'
				? axios
						.get('/api/movies')
						.then(response => {
							this.movies = Object.freeze(response.data);
							this.filteredMovies = response.data;
							this.user = auth;
						})
						.catch(error => console.log(error))
				: this.router.navigate(['/login']);
		});
		/*axios
			.get('/api/loggeduser')
			.then(
				auth =>
					auth.data != 'anonymousUser' && auth.data.username != 'Admin'
						? axios
								.get('/api/movies')
								.then(response => {
									this.movies = Object.freeze(response.data);
									this.filteredMovies = response.data;
									this.user = auth.data;
								})
								.catch(error => console.log(error))
						: this.router.navigate(['/login'])
			)
			.catch(error => this.router.navigate(['/login']));*/
	}
}
