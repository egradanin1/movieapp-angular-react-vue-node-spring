import { Component, OnInit, NgModule } from '@angular/core';
import {
	USER_CONNECTED,
	LOGOUT,
	VERIFY_USER,
	COMMUNITY_CHAT,
	MESSAGE_SENT,
	MESSAGE_RECIEVED,
	TYPING
} from '../../../helpers/Events.js';
import { MessagesComponent } from '../messages/messages.component';
import { MessageinputComponent } from '../messageinput/messageinput.component';
import io from 'socket.io-client';
import { HeaderComponent } from '../header/header.component';
import axios from 'axios';
const socketUrl = 'http://localhost:3232';

@Component({
	selector: 'app-chat',
	templateUrl: './chat.component.html',
	styleUrls: ['./chat.component.css']
})
@NgModule({
	declarations: [ChatComponent, HeaderComponent, MessagesComponent, MessageinputComponent],
	imports: [HeaderComponent, MessageinputComponent, MessagesComponent],
	exports: [ChatComponent],
	providers: []
})
export class ChatComponent implements OnInit {
	socket = null;
	user = null;
	chats = [];
	activeChat = null;
	chat = null;
	isLoaded = false;
	constructor() {}

	ngOnInit() {
		this.initialSetup();
	}
	setUser(principal) {
		let user = principal.user;
		this.socket.emit(USER_CONNECTED, user);
		this.user = user;
	}
	createChat(chat) {
		this.chat = chat;

		const messageEvent = `${MESSAGE_RECIEVED}-${chat.id}`;
		const typingEvent = `${TYPING}-${chat.id}`;

		this.socket.on(typingEvent, this.updateTypingInChat(chat.id));
		this.socket.on(messageEvent, this.addMessageToChat(chat.id));
		axios
			.get('/api/messages')
			.then(response => {
				response.data.map(mes => this.chat.messages.push(mes));
				this.isLoaded = true;
			})
			.catch(error => console.log(error));
	}
	initialSetup() {
		const socket = io(socketUrl);
		this.socket = socket;
		this.socket.on('connect', () => {});
		axios
			.get('/api/loggeduser')
			.then(auth => this.socket.emit(VERIFY_USER, auth.data, this.setUser.bind(this)))
			.catch(error => console.log(error));
		this.socket.emit(COMMUNITY_CHAT, chat => this.createChat(chat));
	}

	addMessageToChat(chatId) {
		return message => {
			this.chat.messages.push(message);
		};
	}
	updateTypingInChat(chatId) {
		return ({ isTyping, user }) => {
			if (user !== this.user.name) {
				if (isTyping && !this.chat.typingUsers.includes(user)) {
					this.chat.typingUsers.push(user);
				} else if (!isTyping && this.chat.typingUsers.includes(user)) {
					this.chat.typingUsers = this.chat.typingUsers.filter(u => u !== user);
				}
			}
		};
	}
	sendMessage(chatId, message) {
		axios
			.post('/api/message', {
				message: message,
				sender: this.user.name,
				time: `${new Date(Date.now()).getHours()}:${('0' + new Date(Date.now()).getMinutes()).slice(-2)}`
			})
			.then(response => console.log(response))
			.catch(error => console.log(error));
		this.socket.emit(MESSAGE_SENT, { chatId, message });
	}
	sendTyping(chatId, isTyping) {
		this.socket.emit(TYPING, { chatId, isTyping });
	}
	sendMessageWrapper(message) {
		this.sendMessage(this.chat.id, message);
	}
	sendTypingWrapper(isTyping) {
		console.log(this);
		this.sendTyping(this.chat.id, isTyping);
	}
}
