import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import axios from 'axios';

@Component({
	selector: 'app-header',
	templateUrl: './header.component.html',
	styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
	constructor(private router: Router) {}

	ngOnInit() {}
	handleClick() {
		axios
			.post('/api/logmeout', {})
			.then(response => this.router.navigate(['/login']))
			.catch(error => console.log(error));
	}
}
