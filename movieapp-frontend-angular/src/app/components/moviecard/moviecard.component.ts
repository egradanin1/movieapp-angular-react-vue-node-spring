import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';
import axios from 'axios';

@Component({
	selector: 'app-moviecard',
	templateUrl: './moviecard.component.html',
	styleUrls: ['./moviecard.component.css']
})
export class MoviecardComponent implements OnInit {
	@Input() movie;
	@Input() user: string;
	constructor(private router: Router) {}

	ngOnInit() {}

	favorite() {
		axios
			.post('/api/favoritemovie', {
				title: this.movie.title,
				username: this.user
			})
			.then(response => this.router.navigate(['/mymovies']))
			.catch(error => console.log(error));
	}
}
