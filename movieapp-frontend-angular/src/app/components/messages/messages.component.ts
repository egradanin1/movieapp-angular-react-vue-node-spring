import { Component, OnInit, ViewChild, ElementRef, Input, AfterViewChecked } from '@angular/core';

@Component({
	selector: 'app-messages',
	templateUrl: './messages.component.html',
	styleUrls: ['./messages.component.css']
})
export class MessagesComponent implements AfterViewChecked {
	@ViewChild('container') container: ElementRef;
	@Input() messages;
	@Input() user;
	@Input() typingUsers;
	constructor() {}

	ngAfterViewInit() {
		this.scrollDown();
	}
	ngAfterViewChecked() {
		this.scrollDown();
	}
	scrollDown() {
		this.container.nativeElement.scrollTop = this.container.nativeElement.scrollHeight;
	}
}
