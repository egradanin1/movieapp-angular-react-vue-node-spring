import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import axios from 'axios';

@Component({
	selector: 'app-adminheader',
	templateUrl: './adminheader.component.html',
	styleUrls: ['./adminheader.component.css']
})
export class AdminheaderComponent implements OnInit {
	constructor(private router: Router) {}

	ngOnInit() {}
	handleClick() {
		axios
			.post('/api/logmeout', {})
			.then(response => this.router.navigate(['/login']))
			.catch(error => console.log(error));
	}
}
