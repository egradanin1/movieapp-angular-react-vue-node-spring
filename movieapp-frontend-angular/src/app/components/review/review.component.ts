import { Component, OnInit, Input } from '@angular/core';

@Component({
	selector: 'app-review',
	templateUrl: './review.component.html',
	styleUrls: ['./review.component.css']
})
export class ReviewComponent implements OnInit {
	@Input() review;
	nRating = [];
	nLeftRating = [];
	constructor() {}

	ngOnInit() {
		this.nRating = Array(this.review.rating).fill(1);
		this.nLeftRating = Array(5 - this.review.rating).fill(1);
	}
	changedReviewRating() {}
}
