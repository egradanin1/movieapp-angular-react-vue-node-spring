import { Component, OnInit, Input } from '@angular/core';

@Component({
	selector: 'app-messageinput',
	templateUrl: './messageinput.component.html',
	styleUrls: ['./messageinput.component.css']
})
export class MessageinputComponent implements OnInit {
	message = '';
	isTyping = false;
	lastUpdateTime = null;
	typingInterval = null;
	@Input() sendMessage;
	@Input() sendTyping;
	constructor() {}

	ngOnInit() {
		this.stopCheckingTyping();
	}

	handleKeyUp(e) {
		if (e.keyCode !== 13) {
			this.sendChatTyping();
		}
	}
	handleChange({ target }) {
		this.message = target.value;
	}

	sendChatMessage() {
		this.sendMessage(this.message);
	}
	handleSubmit(e) {
		e.preventDefault();
		this.sendChatMessage();
		this.message = '';
	}
	sendChatTyping() {
		this.lastUpdateTime = Date.now();
		if (!this.isTyping) {
			this.isTyping = true;
			this.sendTyping(true);
			this.startCheckingTyping();
		}
	}
	startCheckingTyping() {
		console.log('Typing');
		this.typingInterval = setInterval(() => {
			if (Date.now() - this.lastUpdateTime > 300) {
				this.isTyping = false;
				this.stopCheckingTyping();
			}
		}, 300);
	}
	stopCheckingTyping() {
		console.log('Stop Typing');
		if (this.typingInterval) {
			clearInterval(this.typingInterval);
			this.sendTyping(false);
		}
	}
}
