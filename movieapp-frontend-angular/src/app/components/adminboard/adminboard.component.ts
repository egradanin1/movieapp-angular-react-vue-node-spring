import { Component, OnInit, NgModule } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { HttpClientModule } from '@angular/common/http';
import { AdminheaderComponent } from '../adminheader/adminheader.component';
import axios from 'axios';
import { Router } from '@angular/router';

@Component({
	selector: 'app-adminboard',
	templateUrl: './adminboard.component.html',
	styleUrls: ['./adminboard.component.css']
})
@NgModule({
	declarations: [AdminboardComponent, AdminheaderComponent],
	imports: [AdminheaderComponent, HttpClientModule],
	exports: [AdminboardComponent],
	providers: []
})
export class AdminboardComponent implements OnInit {
	users = [];
	filters = {};
	filteredUsers = [];
	constructor(private router: Router, private http: HttpClient) {}

	ngOnInit() {
		this.getUsers();
	}
	handleInput(e) {
		if (e.target.value) {
			if (this.filters && this.filters.hasOwnProperty(e.target.name)) delete this.filters[e.target.name];
			this.filters = Object.assign({ [e.target.name]: e.target.value }, this.filters);
		} else {
			let clone = Object.assign({}, this.filters);
			delete clone[e.target.name];
			this.filters = clone;
		}
		this.filteredUsers = this.users.filter(user =>
			Array.from(Object.keys(this.filters)).every(f =>
				Boolean(~user[f].toLowerCase().indexOf(this.filters[f].toLowerCase()))
			)
		);
	}
	deleteUser(username) {
		axios
			.post('/api/deleteUser', { username: username })
			.then(response => {
				this.users = response.data.filter(el => el.role != 'admin');
				this.filteredUsers = response.data.filter(el => el.role != 'admin');
			})
			.catch(error => console.log(error));
	}
	getUsers() {
		this.http.get('/api/loggeduser', { responseType: 'text' }).subscribe(auth => {
			auth != 'anonymousUser'
				? axios
						.get('/api/getAllUsers', {})
						.then(response => {
							this.users = response.data.filter(el => el.role != 'admin');
							this.filteredUsers = response.data.filter(el => el.role != 'admin');
						})
						.catch(error => console.log(error))
				: this.router.navigate(['/login']);
		});

		/*axios
			.get('/api/loggeduser')
			.then(
				auth =>
					auth.data != 'anonymousUser'
						? axios
								.get('/api/getAllUsers', {})
								.then(response => {
									this.users = response.data.filter(el => el.role != 'admin');
									this.filteredUsers = response.data.filter(el => el.role != 'admin');
								})
								.catch(error => console.log(error))
						: this.router.navigate(['/login'])
			)
			.catch(error => this.router.navigate(['/login']));*/
	}
}
