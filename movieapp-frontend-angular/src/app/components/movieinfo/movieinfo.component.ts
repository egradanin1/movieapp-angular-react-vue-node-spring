import { Component, OnInit, NgModule, Sanitizer } from '@angular/core';
import axios from 'axios';
import { Router, ActivatedRoute } from '@angular/router';
import { HeaderComponent } from '../header/header.component';
import { ReviewComponent } from '../review/review.component';
import { DomSanitizer, SafeResourceUrl, SafeUrl } from '@angular/platform-browser';

@Component({
	selector: 'app-movieinfo',
	templateUrl: './movieinfo.component.html',
	styleUrls: ['./movieinfo.component.css']
})
@NgModule({
	declarations: [MovieinfoComponent, HeaderComponent, ReviewComponent],
	imports: [HeaderComponent, ReviewComponent],
	exports: [MovieinfoComponent],
	providers: []
})
export class MovieinfoComponent implements OnInit {
	movie = null;
	user = null;
	comment = '';
	reviews = [];
	rating = 0;
	iframeURL: SafeResourceUrl = '';
	averageScore = '';
	constructor(private route: ActivatedRoute, private sanitizer: DomSanitizer) {}

	ngOnInit() {
		this.getMovieAndUserInfo();
	}
	getMovieAndUserInfo() {
		axios
			.get(`/api/loggeduser`)
			.then(response => {
				this.user = response.data;
				axios
					.get(`/api/movie/${this.route.snapshot.paramMap.get('title')}`)
					.then(response => {
						this.movie = response.data;
						this.reviews = response.data.reviews;
						let url =
							'https://www.youtube-nocookie.com/embed/' +
							this.movie.video +
							'?rel=0&amp;controls=0&amp;showinfo=0';
						this.iframeURL = this.sanitizer.bypassSecurityTrustResourceUrl(url);
						this.averageScore =
							this.reviews && this.reviews.length
								? (this.reviews.reduce((acc, el) => acc + el.rating, 0) / this.reviews.length).toFixed(
										2
								  ) + '/5'
								: '';
					})
					.catch(error => console.log(error));
			})
			.catch(error => console.log(error));
	}
	handleClick() {
		axios
			.post('/api/review', {
				title: this.movie.title,
				username: this.user,
				rating: this.rating,
				comment: this.comment
			})
			.then(response => {
				this.reviews.push(response.data);
				this.comment = '';
				this.rating = 0;
				this.averageScore =
					this.reviews && this.reviews.length
						? (this.reviews.reduce((acc, el) => acc + el.rating, 0) / this.reviews.length).toFixed(2) + '/5'
						: '';
			})
			.catch(error => console.log(error));
	}
	setRating(rating) {
		this.rating = rating;
	}
	changedRating(e) {
		console.log(this.rating);
	}
}
