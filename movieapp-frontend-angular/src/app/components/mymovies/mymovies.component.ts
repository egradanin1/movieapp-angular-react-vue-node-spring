import { Component, OnInit } from '@angular/core';
import { NgModule } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { HttpClientModule } from '@angular/common/http';
import { MoviecardComponent } from '../moviecard/moviecard.component';
import { HeaderComponent } from '../header/header.component';
import { Router } from '@angular/router';
import axios from 'axios';
import { Movie } from '../../shared/movie';

@Component({
	selector: 'app-mymovies',
	templateUrl: './mymovies.component.html',
	styleUrls: ['./mymovies.component.css']
})
@NgModule({
	declarations: [MymoviesComponent, MoviecardComponent, HeaderComponent],
	imports: [MoviecardComponent, HeaderComponent, HttpClientModule],
	exports: [MymoviesComponent],
	providers: []
})
export class MymoviesComponent implements OnInit {
	movies = [];
	user = null;
	constructor(private router: Router, private http: HttpClient) {}

	ngOnInit() {
		this.getMovies();
	}
	getMovies() {
		this.http.get('/api/loggeduser', { responseType: 'text' }).subscribe(auth => {
			auth != 'anonymousUser'
				? axios
						.get(`/api/movies/${auth}`)
						.then(response => {
							this.movies = Object.freeze(response.data);
							this.user = auth;
							console.log(this.movies, this.user);
						})
						.catch(error => console.log(error))
				: this.router.navigate(['/login']);
		});
		/*axios
			.get('/api/loggeduser')
			.then(
				auth =>
					auth.data != 'anonymousUser'
						? axios
								.get(`/api/movies/${auth.data}`)
								.then(response => {
									this.movies = Object.freeze(response.data);
									this.user = auth.data;
									console.log(this.movies, this.user);
								})
								.catch(error => console.log(error))
						: this.router.navigate(['/login'])
			)
			.catch(error => this.router.navigate(['/login']));
*/
	}
}
