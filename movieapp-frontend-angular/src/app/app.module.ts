import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { AppComponent } from './app.component';
import { LoginComponent } from './components/login/login.component';
import { AppRoutingModule } from './app-routing/app-routing.module';
import { RegisterComponent } from './components/register/register.component';
import { HeaderComponent } from './components/header/header.component';
import { MoviecardComponent } from './components/moviecard/moviecard.component';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { MymoviesComponent } from './components/mymovies/mymovies.component';
import { MovieinfoComponent } from './components/movieinfo/movieinfo.component';
import { StarRatingModule } from 'angular-star-rating';
import { ReviewComponent } from './components/review/review.component';
import { ChatComponent } from './components/chat/chat.component';
import { MessagesComponent } from './components/messages/messages.component';
import { MessageinputComponent } from './components/messageinput/messageinput.component';
import { AdminheaderComponent } from './components/adminheader/adminheader.component';
import { AdminboardComponent } from './components/adminboard/adminboard.component';
import { BenchmarkComponent } from './components/benchmark/benchmark.component';
import { BenchmarkresultsComponent } from './components/benchmarkresults/benchmarkresults.component';

@NgModule({
	declarations: [
		AppComponent,
		LoginComponent,
		RegisterComponent,
		HeaderComponent,
		MoviecardComponent,
		DashboardComponent,
		MymoviesComponent,
		MovieinfoComponent,
		ReviewComponent,
		ChatComponent,
		MessagesComponent,
		MessageinputComponent,
		AdminheaderComponent,
		AdminboardComponent,
		BenchmarkComponent,
		BenchmarkresultsComponent
	],
	imports: [BrowserModule, AppRoutingModule, FormsModule, StarRatingModule.forRoot(), HttpClientModule],
	entryComponents: [LoginComponent],
	providers: [],
	bootstrap: [AppComponent]
})
export class AppModule {}
