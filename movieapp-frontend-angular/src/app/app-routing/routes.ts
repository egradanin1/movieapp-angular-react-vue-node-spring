import { Routes } from '@angular/router';

import { LoginComponent } from '../components/login/login.component';
import { RegisterComponent } from '../components/register/register.component';
import { DashboardComponent } from '../components/dashboard/dashboard.component';
import { MymoviesComponent } from '../components/mymovies/mymovies.component';
import { MovieinfoComponent } from '../components/movieinfo/movieinfo.component';
import { ChatComponent } from '../components/chat/chat.component';
import { AdminboardComponent } from '../components/adminboard/adminboard.component';
import { BenchmarkComponent } from '../components/benchmark/benchmark.component';
import { BenchmarkresultsComponent } from '../components/benchmarkresults/benchmarkresults.component';

export const routes: Routes = [
	{ path: 'login', component: LoginComponent },
	{ path: 'register', component: RegisterComponent },
	{ path: 'dashboard', component: DashboardComponent },
	{ path: 'mymovies', component: MymoviesComponent },
	{ path: 'details/:title', component: MovieinfoComponent },
	{ path: 'chat', component: ChatComponent },
	{ path: 'adminboard', component: AdminboardComponent },
	{ path: 'benchmark', component: BenchmarkComponent },
	{ path: 'benchmarkResults', component: BenchmarkresultsComponent },
	{ path: '', redirectTo: 'login', pathMatch: 'full' }
];
