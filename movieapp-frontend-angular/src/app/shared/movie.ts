export class Movie {
	id: string;
	title: string;
	description: string;
	image: string;
	video: string;
	publishedYear: string;
	reviews: null;
	users: null;
}
