# Movieapp
A project to demonstrate how a web app can be built using a Spring Boot framework and mongo database with three different frontend frameworks Angular6, Vue, React and a comparative analysis of three already mentioned frameworks. 

### Installation dependencies ###

The following dependencies are necessary: 

 - Java 8
 - Node
 - npm
 - maven 3
 - mongo


### Building and starting the server ###

To build the backend and start the server, navigate to  the movieapp-spring-backend/Movieapp directory and run the following command:

    mvn spring-boot:run

After the server starts it will be accessible on port 8080 and a admin user will be created in mongo database (movieappdb) with following credentials:

    http://localhost:8080/
	username: Admin
	password: Admin123


### Building and starting the chat-socket-server ###

Navigate to chat-socket-server directory and run 

    npm install

To start the server run the following command: (nodemon will be installed locally) 

  	npm start

After the server starts it will be accessible on port 3232
	
### Installing frontend dependencies and running frontend frameworks ###

After cloning the repository, navigate in the terminal to the directory of wanted framework and run the following command:

    npm install
	
To start type (for each framework including angular don't use "ng serve" because of the backend proxy):	

	npm start

The frontend frameworks will be running on the following ports

 - Angular 4200
 - React 3000
 - Vue 4545
 
 Navigate to register page and create new user, every field will be validated.


