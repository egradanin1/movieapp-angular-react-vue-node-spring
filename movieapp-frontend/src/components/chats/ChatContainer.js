import React, { Component } from 'react';
import {
	USER_CONNECTED,
	LOGOUT,
	VERIFY_USER,
	COMMUNITY_CHAT,
	MESSAGE_SENT,
	MESSAGE_RECIEVED,
	TYPING
} from '../../helpers/Events';
import Messages from '../messages/Messages';
import MessageInput from '../messageinput/MessageInput';
import './ChatContainer.css';
import Header from '../header/Header';
import io from 'socket.io-client';
import axios from 'axios';

const socketUrl = 'http://localhost:3232';
class ChatContainer extends Component {
	state = {
		socket: null,
		user: null,
		chats: [],
		activeChat: null,
		chat: null
	};
	componentDidMount() {
		const socket = io(socketUrl);

		socket.on('connect', () => {});

		this.setState({ socket });

		axios
			.get('/loggeduser')
			.then(auth => socket.emit(VERIFY_USER, auth.data, this.setUser))
			.catch(error => console.log(error));
		socket.emit(COMMUNITY_CHAT, chat => this.createChat(chat));
	}
	setUser = principal => {
		console.log(principal);
		let user = principal.user;
		const { socket } = this.state;
		socket.emit(USER_CONNECTED, user);
		this.setState({ user });
	};

	createChat = chat => {
		const { socket } = this.state;
		this.setState({ chat });

		const messageEvent = `${MESSAGE_RECIEVED}-${chat.id}`;
		const typingEvent = `${TYPING}-${chat.id}`;

		socket.on(typingEvent, this.updateTypingInChat(chat.id));
		socket.on(messageEvent, this.addMessageToChat(chat.id));
		axios
			.get('/messages')
			.then(response => {
				response.data.map(mes => chat.messages.push(mes));
				this.setState({ chat });
			})
			.catch(error => console.log(error));
	};

	addMessageToChat = chatId => {
		return message => {
			const { chat } = this.state;
			chat.messages.push(message);
			this.setState({ chat });
		};
	};
	updateTypingInChat = chatId => {
		return ({ isTyping, user }) => {
			if (user !== this.state.user.name) {
				const { chat } = this.state;

				if (isTyping && !chat.typingUsers.includes(user)) {
					chat.typingUsers.push(user);
				} else if (!isTyping && chat.typingUsers.includes(user)) {
					chat.typingUsers = chat.typingUsers.filter(u => u !== user);
				}
				this.setState({ chat });
			}
		};
	};

	sendMessage = (chatId, message) => {
		const { socket, user } = this.state;
		axios
			.post('/message', {
				message: message,
				sender: user.name,
				time: `${new Date(Date.now()).getHours()}:${('0' + new Date(Date.now()).getMinutes()).slice(-2)}`
			})
			.then(response => console.log(response))
			.catch(error => console.log(error));
		socket.emit(MESSAGE_SENT, { chatId, message });
	};

	sendTyping = (chatId, isTyping) => {
		const { socket } = this.state;
		socket.emit(TYPING, { chatId, isTyping });
	};

	render() {
		const { user, chat } = this.state;
		if (!chat) return <div>Loading!!!</div>;
		return (
			<div>
				<Header />
				<div className="container">
					<div className="chat-room-container">
						<Messages messages={chat.messages} user={user} typingUsers={chat.typingUsers} />
						<MessageInput
							sendMessage={message => {
								this.sendMessage(chat.id, message);
							}}
							sendTyping={isTyping => {
								this.sendTyping(chat.id, isTyping);
							}}
						/>
					</div>
				</div>
			</div>
		);
	}
}
export default ChatContainer;
