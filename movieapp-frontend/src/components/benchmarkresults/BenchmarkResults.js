import React, { Component } from 'react';
import './BenchmarkResults.css';
import Adminheader from '../adminheader/Adminheader';
import axios from 'axios';

class BenchmarkResults extends Component {
	state = {
		benchmarks: [],
		filteredBenchmarks: [],
		filters: {},
		averageFrameworkScores: ''
	};

	componentDidMount() {
		axios
			.get('/benchmarks')
			.then(response =>
				this.setState({
					benchmarks: response.data,
					filteredBenchmarks: response.data
				})
			)
			.catch(error => console.log(error));
	}
	handleInput(e) {
		if (e.target.value) {
			if (this.state.filters && this.state.filters.hasOwnProperty(e.target.name))
				delete this.state.filters[e.target.name];
			this.state.filters = Object.assign({ [e.target.name]: e.target.value }, this.state.filters);
		} else {
			delete this.state.filters[e.target.name];
		}
		this.setState({
			filteredBenchmarks: this.state.benchmarks.filter(benchmark =>
				Array.from(Object.keys(this.state.filters)).every(
					f =>
						f !== 'duration'
							? ~benchmark[f].toLowerCase().indexOf(this.state.filters[f].toLowerCase())
							: Number(benchmark[f]) < Number(this.state.filters[f])
				)
			)
		});
	}

	averageScores(framework) {
		let { benchmarks } = this.state;
		let averageFrameworkScores = '';
		let groupedBenchmarksByAction;
		Array.from(
			Object.keys(
				(groupedBenchmarksByAction = benchmarks.filter(el => el.framework === framework).reduce((acc, el) => {
					acc[el.action] = acc[el.action] || [];
					acc[el.action].push(el);
					return acc;
				}, Object.create(null)))
			)
		).map(
			el =>
				(averageFrameworkScores += `${el} ---> ${(
					groupedBenchmarksByAction[el].reduce((acc, score) => acc + Number(score.duration), 0) /
					groupedBenchmarksByAction[el].length
				).toFixed(2)} ms <br/>`)
		);
		this.setState({ averageFrameworkScores });
		document.getElementById('averageScore-container').innerHTML = averageFrameworkScores;
	}

	render() {
		let { filteredBenchmarks, benchmarks, averageFrameworkScores } = this.state;
		let header = (
			<tr id="header">
				<th>Action</th>
				<th>Duration</th>
				<th>Framework</th>
			</tr>
		);
		let filter = (
			<tr id="filter">
				<td>
					<input type="text" name="action" placeholder="Action" onChange={e => this.handleInput(e)} />
				</td>
				<td>
					<input type="text" name="duration" placeholder=" > duration" onChange={e => this.handleInput(e)} />
				</td>
				<td>
					<input type="text" name="framework" placeholder="framework" onChange={e => this.handleInput(e)} />
				</td>
			</tr>
		);
		let rows = filteredBenchmarks.map((benchmark, index) => (
			<tr key={index} className="rows">
				<td>{benchmark.action}</td>
				<td>{benchmark.duration}</td>
				<td>{benchmark.framework}</td>
			</tr>
		));
		return (
			<div>
				<Adminheader />
				<div className="framework-option-container">
					<button onClick={e => this.averageScores('angular')}>Angular Average Scores</button>
					<button onClick={e => this.averageScores('react')}>React Average Scores</button>
					<button onClick={e => this.averageScores('vue')}>Vue Average Scores</button>
				</div>
				<div hidden={!averageFrameworkScores} id="averageScore-container" value={averageFrameworkScores} />
				<div className="table-container">
					<table hidden={!benchmarks.length} id="benchmarkResultsTable">
						<tbody>
							{header}
							{filter}
							{rows}
						</tbody>
					</table>
				</div>
			</div>
		);
	}
}

export default BenchmarkResults;
