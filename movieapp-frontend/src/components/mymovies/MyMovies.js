import React, { Component } from 'react';
import { Link, Redirect } from 'react-router-dom';
import MovieCard from '../moviecard/MovieCard';
import Header from '../header/Header';
import './MyMovies.css';
import axios from 'axios';

class MyMovies extends Component {
	state = {
		toLogin: false,
		unauthorized: false,
		movies: [],
		user: ''
	};
	componentWillMount() {
		axios
			.get('/loggeduser')
			.then(
				auth =>
					auth.data != 'anonymousUser'
						? axios
								.get(`movies/${auth.data}`)
								.then(response => this.setState({ movies: response.data, user: auth.data }))
								.catch(error => console.log(error))
						: this.setState({ toLogin: true })
			)
			.catch(error => this.setState({ unauthorized: true }));
	}
	render() {
		let { toLogin, unauthorized, movies, user } = this.state;
		if (toLogin) {
			return <Redirect to="/login" />;
		}
		if (unauthorized) {
			return <Redirect to="/login" />;
		}
		return (
			<div>
				<Header />
				<div className="movie-list">
					{movies.map((movie, index) => <MovieCard {...movie} user={user} key={movie.id} id={index} />)}
				</div>
			</div>
		);
	}
}

export default MyMovies;
