import React, { Component } from 'react';
import { Link, Redirect } from 'react-router-dom';
import './Header.css';
import axios from 'axios';

class Header extends Component {
	state = {
		toLogin: false
	};
	handleClick() {
		axios
			.post('/logmeout', {})
			.then(response => this.setState({ toLogin: true }))
			.catch(error => console.log(error));
	}
	render() {
		if (this.state.toLogin) return <Redirect to="/login" />;
		return (
			<header className="App-header">
				<nav>
					<ul id="menu">
						<li>
							<Link to="/">Trending Movie</Link>
						</li>
						<li>
							<Link to="/mymovies">My Movies</Link>
						</li>
						<li>
							<Link to="/chat">Chat</Link>
						</li>
						<button onClick={e => this.handleClick()}>Logout</button>
					</ul>
				</nav>
			</header>
		);
	}
}

export default Header;
