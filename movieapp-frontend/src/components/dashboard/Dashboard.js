import React, { Component } from 'react';
import { Link, Redirect } from 'react-router-dom';
import MovieCard from '../moviecard/MovieCard';
import Header from '../header/Header';
import './Dashboard.css';
import axios from 'axios';

class Dashboard extends Component {
	state = {
		toLogin: false,
		unauthorized: false,
		movies: [],
		user: '',
		filteredMovies: [],
		filter: ''
	};
	componentWillMount() {
		axios
			.get('/loggeduser')
			.then(
				auth =>
					auth.data != 'anonymousUser' && auth.data.username != 'Admin'
						? axios
								.get('movies')
								.then(response =>
									this.setState({
										movies: response.data,
										filteredMovies: response.data,
										user: auth.data
									})
								)
								.catch(error => console.log(error))
						: this.setState({ toLogin: true })
			)
			.catch(error => this.setState({ unauthorized: true }));
	}
	handleInput(e) {
		this.setState({
			filteredMovies: this.state.movies.filter(
				movie => ~movie.title.toLowerCase().indexOf(e.target.value.toLowerCase())
			),
			filter: e.target.value
		});
	}
	render() {
		let { toLogin, unauthorized, movies, user, filteredMovies, filter } = this.state;
		if (toLogin) {
			return <Redirect to="/login" />;
		}
		if (unauthorized) {
			return <Redirect to="/login" />;
		}
		return (
			<div>
				<Header />
				<input
					type="text"
					value={filter}
					id="filter-input"
					onChange={e => this.handleInput(e)}
					placeholder="Search"
				/>
				<div className="movie-list">
					{filteredMovies.map((movie, index) => (
						<MovieCard {...movie} user={user} key={movie.id} id={index} />
					))}
				</div>
			</div>
		);
	}
}

export default Dashboard;
