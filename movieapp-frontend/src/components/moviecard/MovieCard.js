import React, { Component } from 'react';
import { Link, Redirect } from 'react-router-dom';
import './MovieCard.css';
import axios from 'axios';

class MovieCard extends Component {
	state = {
		toMyMovies: false
	};
	favorite() {
		axios
			.post('/favoritemovie', {
				title: this.props.title,
				username: this.props.user
			})
			.then(response => this.setState({ toMyMovies: true }))
			.catch(error => console.log(error));
	}
	render() {
		if (this.state.toMyMovies) return <Redirect to="/mymovies" />;
		return (
			<div className="movie-item">
				<Link to={`/details/${this.props.title}`}>
					<img alt={this.props.title} src={this.props.image} />
					<div className="basic-info">
						<h3>{this.props.title}</h3>
						<h4>({this.props.publishedYear})</h4>
					</div>
				</Link>
				<div className="favorite">
					<p>{this.props.description}</p>
					<button hidden={~this.props.users.indexOf(this.props.user)} onClick={e => this.favorite()}>
						Favorite
					</button>
				</div>
			</div>
		);
	}
}

export default MovieCard;
