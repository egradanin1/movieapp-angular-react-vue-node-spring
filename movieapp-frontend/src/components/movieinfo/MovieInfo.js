import React, { Component } from 'react';
import { Link, Redirect } from 'react-router-dom';
import StarRatingComponent from 'react-star-rating-component';
import Header from '../header/Header';
import Review from '../review/Review';
import './MovieInfo.css';
import axios from 'axios';

class MovieInfo extends Component {
	state = {
		movie: null,
		user: null,
		comment: '',
		reviews: [],
		rating: 0
	};
	componentWillMount() {
		axios
			.get(`/movie/${this.props.match.params.title}`)
			.then(response => this.setState({ movie: response.data, reviews: response.data.reviews }))
			.catch(error => console.log(error));
		axios
			.get(`/loggeduser`)
			.then(response => this.setState({ user: response.data }))
			.catch(error => console.log(error));
	}
	handleInput(e) {
		this.setState({ comment: e.target.value });
	}
	onStarClick(nextValue, prevValue, name) {
		this.setState({ rating: nextValue });
	}
	handleClick() {
		axios
			.post('/review', {
				title: this.state.movie.title,
				username: this.state.user,
				rating: this.state.rating,
				comment: this.state.comment
			})
			.then(response => {
				let reviews = [...this.state.reviews, response.data];
				this.setState({ reviews, comment: '', rating: 0 });
			})
			.catch(error => console.log(error));
	}
	render() {
		let { movie, comment, rating, reviews } = this.state;
		if (!movie) return <div>Loading!!!</div>;
		return (
			<div>
				<Header />
				<div className="details">
					<div className="info">
						<img src={movie.image} alt={movie.title} />
						<div className="title">
							<h3>{movie.title}</h3>
							<h4>({movie.publishedYear})</h4>
						</div>
						<p>{movie.description}</p>
					</div>
					<div className="reviews">
						<div className="comments">
							<p>
								{reviews && reviews.length
									? (reviews.reduce((acc, el) => acc + el.rating, 0) / reviews.length).toFixed(2) +
									  '/5'
									: ''}
							</p>
							<div className="reviewsBox">
								{reviews
									.slice()
									.reverse()
									.map((el, index) => <Review key={index} {...el} />)}
							</div>
						</div>

						<div className="add-review">
							<input
								value={comment}
								onChange={e => this.handleInput(e)}
								type="text"
								placeholder="Add comment"
							/>
							<StarRatingComponent
								name="rate1"
								starCount={5}
								value={rating}
								onStarClick={this.onStarClick.bind(this)}
							/>
							<button onClick={e => this.handleClick()}>Add review</button>
						</div>
					</div>

					<div className="trailer">
						<iframe
							title="YouTube Video Frame"
							src={`https://www.youtube-nocookie.com/embed/${
								movie.video
							}?rel=0&amp;controls=0&amp;showinfo=0`}
							frameBorder="0"
							allowFullScreen
						/>
					</div>
				</div>
			</div>
		);
	}
}

export default MovieInfo;
