import React, { Component } from 'react';
import { Link, Redirect } from 'react-router-dom';
import './Adminheader.css';
import axios from 'axios';

class Adminheader extends Component {
	state = {
		toLogin: false
	};
	handleClick() {
		axios
			.post('/logmeout', {})
			.then(response => this.setState({ toLogin: true }))
			.catch(error => console.log(error));
	}
	render() {
		if (this.state.toLogin) return <Redirect to="/login" />;
		return (
			<header className="App-header">
				<nav>
					<ul id="menu">
						<li>
							<Link to="/adminboard">Manage Users</Link>
						</li>
						<li>
							<Link to="/benchmark">Benchmark</Link>
						</li>
						<li>
							<Link to="/benchmarkResults">Benchmark Results</Link>
						</li>
						<button onClick={e => this.handleClick()}>Logout</button>
					</ul>
				</nav>
			</header>
		);
	}
}

export default Adminheader;
