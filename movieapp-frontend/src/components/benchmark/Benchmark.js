import React, { Component } from 'react';
import './Benchmark.css';
import Adminheader from '../adminheader/Adminheader';
import Options from '../../helpers/helperfunctions';
import axios from 'axios';

var startTime;
var endTime;
var actionName;
var duration;

class Benchmark extends Component {
	state = {
		shows: []
	};
	calculateDuration() {
		window.setTimeout(() => {
			endTime = performance.now();
			duration = endTime - startTime;
			console.log(duration, actionName);
			axios
				.post('/benchmark', {
					action: actionName,
					duration: duration,
					framework: 'react'
				})
				.then(response => console.log(response))
				.catch(error => console.log(error));
		}, 0);
	}
	componentDidUpdate() {
		this.calculateDuration();
	}
	measureTime(fun) {
		actionName = fun.name;
		startTime = performance.now();
		fun.bind(this)();
	}
	deleteShow(index) {
		this.setState({ shows: Options.deleteRow(index) });
	}
	createShows() {
		let { shows } = this.state;
		if (!shows.length) {
			this.setState({ shows: Options.makeData(500) });
		}
	}
	addShows() {
		this.setState({ shows: Options.makeData(500) });
	}
	updateShows() {
		this.setState({ shows: Options.updateData() });
	}
	deleteShows() {
		this.setState({ shows: Options.deleteData() });
	}
	render() {
		let { shows } = this.state;
		let header = (
			<tr id="header">
				<th>Row Number</th>
				<th>Show Title</th>
				<th>Show Genre</th>
				<th>Show Actor</th>
				<th>Delete Show</th>
			</tr>
		);
		let rows = shows.map((show, index) => (
			<tr key={index} className="rows">
				<td>{index + 1}</td>
				<td>{show.title}</td>
				<td>{show.genre}</td>
				<td>{show.actor}</td>
				<td>
					<a className="delete-link" onClick={() => this.deleteShow(index)}>
						Delete
					</a>
				</td>
			</tr>
		));
		return (
			<div>
				<Adminheader />
				<div className="options-container">
					<button onClick={e => this.measureTime(this.createShows)}>Create 500 shows</button>
					<button onClick={e => this.measureTime(this.addShows)}>Add 500 shows</button>
					<button onClick={e => this.measureTime(this.updateShows)}>Update rows</button>
					<button onClick={e => this.measureTime(this.deleteShows)}>Delete shows</button>
				</div>
				<div className="table-container">
					<table hidden={!shows.length} id="benchmarkTable">
						<tbody>
							{header}
							{rows}
						</tbody>
					</table>
				</div>
			</div>
		);
	}
}

export default Benchmark;
