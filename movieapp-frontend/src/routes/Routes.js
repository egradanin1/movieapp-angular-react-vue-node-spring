import React from 'react';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import App from '../App';
import Dashboard from '../components/dashboard/Dashboard';
import Adminboard from '../components/adminboard/Adminboard';
import Login from '../components/login/Login';
import Register from '../components/register/Register';
import MyMovies from '../components/mymovies/MyMovies';
import MovieInfo from '../components/movieinfo/MovieInfo';
import ChatContainer from '../components/chats/ChatContainer';
import Benchmark from '../components/benchmark/Benchmark';
import BenchmarkResults from '../components/benchmarkresults/BenchmarkResults';
export default (
  <Router>
    <App>
      <Route path="/dashboard" component={Dashboard} />
      <Route path="/adminboard" component={Adminboard} />
      <Route path="/login" component={Login} />
      <Route path="/register" component={Register} />
      <Route path="/mymovies" component={MyMovies} />
      <Route path="/chat" component={ChatContainer} />
      <Route path="/benchmark" component={Benchmark} />
      <Route path="/benchmarkResults" component={BenchmarkResults} />
      <Route path="/details/:title" component={MovieInfo} />
      <Route exact={true} path="/" component={Dashboard} />
    </App>
  </Router>
);
