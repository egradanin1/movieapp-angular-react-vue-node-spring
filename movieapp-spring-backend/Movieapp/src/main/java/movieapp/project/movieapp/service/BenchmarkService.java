package movieapp.project.movieapp.service;

import movieapp.project.movieapp.Domain.Models.Benchmark;
import java.util.List;

public interface BenchmarkService {
    void save(Benchmark benchmark);
    List<Benchmark> getAllBenchmarks();
}
