package movieapp.project.movieapp.Controllers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import movieapp.project.movieapp.Domain.Models.User;
import movieapp.project.movieapp.Domain.Models.Movie;
import movieapp.project.movieapp.Domain.Models.Review;
import movieapp.project.movieapp.Domain.Models.ChatMessage;
import movieapp.project.movieapp.Domain.Models.Benchmark;
import movieapp.project.movieapp.Repositories.UserRepository;
import movieapp.project.movieapp.Repositories.ChatMessageRepository;
import movieapp.project.movieapp.Repositories.BenchmarkRepository;
import movieapp.project.movieapp.service.SecurityService;
import movieapp.project.movieapp.service.UserService;
import movieapp.project.movieapp.service.MovieService;
import movieapp.project.movieapp.service.ChatMessageService;
import movieapp.project.movieapp.service.BenchmarkService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.http.MediaType;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import java.util.List;
import java.util.ArrayList;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.core.Authentication;
import java.time.LocalDateTime;
import java.util.UUID;


@RestController
public class DefaultController {
    @Autowired
    private UserService userService;

    @Autowired
    private MovieService movieService;

    @Autowired
    private ChatMessageService chatMessageService;

    @Autowired
    private BenchmarkService benchmarkService;

    @Autowired
    private SecurityService securityService;

    @Autowired
    private UserRepository userRepository;

    private static final Logger logger = LoggerFactory.getLogger(DefaultController.class);

    @RequestMapping(value = "/registration", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public void registration(@RequestBody User userData) {
        BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
        User user = new User();
        user.setCreated(LocalDateTime.now());
        user.setId(UUID.randomUUID());
        user.setPassword(encoder.encode(userData.getPassword()));
        user.setUsername(userData.getUsername());
        user.setFirstName(userData.getFirstName());
        user.setLastName(userData.getLastName());
        user.setEmail(userData.getEmail());
        user.setMovies(new ArrayList<Movie>());
        user.setRole("user");
        userRepository.save(user);
        securityService.autologin(user.getUsername(), userData.getPassword());
    }

    @RequestMapping(value = {"/", "/welcome"}, method = RequestMethod.GET)
    public String welcome(Model model) {
        return "Welcome, you are logged in!";
    }

    @RequestMapping(value = "/login", method = RequestMethod.POST)
    public User login(@RequestBody User userData) {
       BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
       User user = userRepository.findByUsername(userData.getUsername());
       if (user == null) return null;
       logger.error(String.format("Auto login %s successfully!", user.getUsername() + " " + user.getPassword() + " " + userData.getPassword()));

       if( user !=  null && encoder.matches(userData.getPassword(),user.getPassword()) ) {
            logger.error(String.format("Auto login %s successfully!", user.getUsername() + " " + userData.getPassword()));
            securityService.autologin(user.getUsername(),userData.getPassword());
       }
       

       return user;
    } 

    @RequestMapping(value="/logmeout", method = RequestMethod.POST)
    public void logoutPage (HttpServletRequest request, HttpServletResponse response) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        if (auth != null){
        new SecurityContextLogoutHandler().logout(request, response, auth);
        }
    }  

    @RequestMapping(value="/loggeduser", method = RequestMethod.GET)
    public Object loggeduser (HttpServletRequest request, HttpServletResponse response) {
        return SecurityContextHolder.getContext().getAuthentication().getPrincipal();      
        
    } 

    @RequestMapping(value="/getAllUsers", method = RequestMethod.GET)
    public List<User> getAllUsers () {
         return userService.getAllUsers();     
        
    } 

    @RequestMapping(value = "/deleteUser", method = RequestMethod.POST)
    public List<User> deleteUser(@RequestBody User userData) {
        userService.deleteUser(userService.findByUsername(userData.getUsername()));
        return userService.getAllUsers(); 
    } 

    @RequestMapping(value="/movies/{username}", method = RequestMethod.GET)
    public List<Movie> getAllMoviesOfUser (@PathVariable String username) {
         return userService.findByUsername(username).getMovies();     
        
    }

    @RequestMapping(value="/movies", method = RequestMethod.GET)
    public List<Movie> getAllMovies () {
         return movieService.getAllMovies();    
        
    }

    @RequestMapping(value="/movie/{title}", method = RequestMethod.GET)
    public Movie getMovie (@PathVariable String title) {
         return movieService.findByTitle(title);     
    }
    

    @RequestMapping(value = "/review", method = RequestMethod.POST)
    public Review review(@RequestBody Review reviewData) {
        Movie movie = movieService.findByTitle(reviewData.getTitle());
        List<Review> listOfReviews = movie.getReviews(); 
        listOfReviews.add(reviewData);
        movie.setReviews(listOfReviews);
        movieService.save(movie);
        return reviewData;
    } 

    @RequestMapping(value = "/favoritemovie", method = RequestMethod.POST)
    public void favoriteMovie(@RequestBody Review reviewData) {
        Movie movie = movieService.findByTitle(reviewData.getTitle());
        User user = userService.findByUsername(reviewData.getUsername());
        List<Movie> listOfMovies = user.getMovies(); 
        listOfMovies.add(movie);
        List<String> listOfUsers = movie.getUsers(); 
        listOfUsers.add(user.getUsername());
        user.setMovies(listOfMovies);
        movie.setUsers(listOfUsers);
        userService.save(user);
        movieService.save(movie);
    } 


    @RequestMapping(value = "/message", method = RequestMethod.POST)
    public ChatMessage message(@RequestBody ChatMessage chatMessageData) {
        chatMessageData.setId(UUID.randomUUID());
        chatMessageService.save(chatMessageData);
        return chatMessageData;
    } 

    @RequestMapping(value="/messages", method = RequestMethod.GET)
    public List<ChatMessage> messages () {
         return chatMessageService.getAllChatMessages();    
    }

    @RequestMapping(value = "/benchmark", method = RequestMethod.POST)
    public Benchmark benchmark(@RequestBody Benchmark benchmarkData) {
        benchmarkData.setId(UUID.randomUUID());
        benchmarkService.save(benchmarkData);
        return benchmarkData;
    } 

    @RequestMapping(value="/benchmarks", method = RequestMethod.GET)
    public List<Benchmark> benchmarks () {
         return benchmarkService.getAllBenchmarks();    
    }
    
}
