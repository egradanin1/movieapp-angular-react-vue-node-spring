package movieapp.project.movieapp.service;

import movieapp.project.movieapp.Domain.Models.ChatMessage;
import movieapp.project.movieapp.Repositories.ChatMessageRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.Arrays;
import java.util.List;

@Service
public class ChatMessageServiceImpl implements ChatMessageService {
    @Autowired
    private ChatMessageRepository chatMessageRepository;


    @Override
    public void save(ChatMessage chatMessage) {
        chatMessageRepository.save(chatMessage);
    }

    @Override
    public List<ChatMessage> getAllChatMessages() {
        return chatMessageRepository.findAll();
    }
}
