package movieapp.project.movieapp.Configuration;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import movieapp.project.movieapp.Domain.Models.User;
import movieapp.project.movieapp.Repositories.UserRepository;
import movieapp.project.movieapp.service.UserService;
import movieapp.project.movieapp.Domain.Models.Movie;
import movieapp.project.movieapp.Repositories.MovieRepository;
import movieapp.project.movieapp.service.MovieService;
import movieapp.project.movieapp.service.SecurityService;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import movieapp.project.movieapp.restAuth.RESTAuthenticationEntryPoint;
import movieapp.project.movieapp.restAuth.RESTAuthenticationFailureHandler;
import movieapp.project.movieapp.restAuth.RESTAuthenticationSuccessHandler;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.cors.CorsConfiguration;
import java.util.HashSet;
import java.util.Arrays;
import java.util.ArrayList;
import java.time.LocalDateTime;
import java.util.UUID;
import org.springframework.web.servlet.config.annotation.CorsRegistry;

@Configuration
@EnableWebSecurity
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {

	@Autowired
    private RESTAuthenticationEntryPoint authenticationEntryPoint;
    @Autowired
    private RESTAuthenticationFailureHandler authenticationFailureHandler;
    @Autowired
    private RESTAuthenticationSuccessHandler authenticationSuccessHandler;

    @Autowired
    private UserDetailsService userDetailsService;

    @Autowired
    private UserService userService;

    @Autowired
    private UserRepository userRepository;


    @Autowired
    private MovieService movieService;

    @Autowired
    private MovieRepository movieRepository;

    private static final Logger logger = LoggerFactory.getLogger(SecurityConfiguration.class);


    @Bean
    public BCryptPasswordEncoder bCryptPasswordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Bean
    @Override
    public AuthenticationManager authenticationManagerBean() throws Exception {
       return super.authenticationManagerBean();
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.authorizeRequests().antMatchers("/welcome").authenticated();
        http.exceptionHandling().authenticationEntryPoint(authenticationEntryPoint);
        http.csrf().disable();
        //http.cors().configurationSource(request -> new CorsConfiguration().applyPermitDefaultValues());
    }

    private void storeMoviesInDatabase(){
       if (movieService.getAllMovies().isEmpty()){
            Movie movie = new Movie("Deadpool 2","Foul-mouthed mutant mercenary Wade Wilson (AKA. Deadpool), brings together a team of fellow mutant rogues to protect a young boy with supernatural abilities from the brutal, time-traveling cyborg, Cable.","https://m.media-amazon.com/images/M/MV5BMjI3Njg3MzAxNF5BMl5BanBnXkFtZTgwNjI2OTY0NTM@._V1_SY1000_CR0,0,674,1000_AL_.jpg","D86RtevtfrA","2018");
            movie.setId(UUID.randomUUID());
            movieService.save(movie);
            movie = new Movie("Avengers: Infinity War","The Avengers and their allies must be willing to sacrifice all in an attempt to defeat the powerful Thanos before his blitz of devastation and ruin puts an end to the universe.","https://ia.media-imdb.com/images/M/MV5BMjMxNjY2MDU1OV5BMl5BanBnXkFtZTgwNzY1MTUwNTM@._V1_SY1000_CR0,0,674,1000_AL_.jpg","6ZfuNTqbHE8","2018");
            movie.setId(UUID.randomUUID());
            movieService.save(movie);
            movie = new Movie("Black Panther", "T'Challa, the King of Wakanda, rises to the throne in the isolated, technologically advanced African nation, but his claim is challenged by a vengeful outsider who was a childhood victim of T'Challa's father's mistake.", "https://m.media-amazon.com/images/M/MV5BMTg1MTY2MjYzNV5BMl5BanBnXkFtZTgwMTc4NTMwNDI@._V1_SY1000_CR0,0,674,1000_AL_.jpg", "xjDjIWPwcPU", "2018");
            movie.setId(UUID.randomUUID());
            movieService.save(movie);
            movie = new Movie("Annihilation", "A biologist signs up for a dangerous, secret expedition into a mysterious zone where the laws of nature don't apply.", "https://m.media-amazon.com/images/M/MV5BMTk2Mjc2NzYxNl5BMl5BanBnXkFtZTgwMTA2OTA1NDM@._V1_SY1000_CR0,0,640,1000_AL_.jpg", "89OP78l9oF0", "2018");
            movie.setId(UUID.randomUUID());
            movieService.save(movie);
            movie =  new Movie("Red Sparrow", "Ballerina Dominika Egorova is recruited to 'Sparrow School,' a Russian intelligence service where she is forced to use her body as a weapon.", "https://m.media-amazon.com/images/M/MV5BMTA3MDkxOTc4NDdeQTJeQWpwZ15BbWU4MDAxNzgyNTQz._V1_SY1000_CR0,0,674,1000_AL_.jpg", "PmUL6wMpMWw", "2018");
            movie.setId(UUID.randomUUID());
            movieService.save(movie);
       }       
    }

    @Autowired
    public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(userDetailsService).passwordEncoder(bCryptPasswordEncoder());
        if (userRepository.findByRole("admin") == null){
            User user = new User();
            user.setCreated(LocalDateTime.now());
            user.setId(UUID.randomUUID());
            user.setPassword(bCryptPasswordEncoder().encode("Admin123"));
            user.setUsername("Admin");
            user.setRole("admin");
            user.setMovies(new ArrayList<Movie>());
            userRepository.save(user);
        }
        storeMoviesInDatabase();
    }
}
