package movieapp.project.movieapp.Domain.Models;

import java.util.UUID;
import javax.persistence.*;
import java.util.Set;
import java.time.LocalDateTime;
import org.springframework.data.annotation.Id;



public class ChatMessage extends BaseModel{


    private String message;
    private String sender;
    private String time;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getSender() {
        return sender;
    }

    public void setSender(String sender) {
        this.sender = sender;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

	public ChatMessage(String message, String sender, String time) {
	    this.message = message;
	    this.sender = sender;
	    this.time = time;
	}
    public ChatMessage () {}
}
