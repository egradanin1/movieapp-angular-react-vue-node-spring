package movieapp.project.movieapp.service;

import movieapp.project.movieapp.Domain.Models.Benchmark;
import movieapp.project.movieapp.Repositories.BenchmarkRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.Arrays;
import java.util.List;

@Service
public class BenchmarkServiceImpl implements BenchmarkService {
    @Autowired
    private BenchmarkRepository benchmarkRepository;


    @Override
    public void save(Benchmark benchmark) {
        benchmarkRepository.save(benchmark);
    }

    @Override
    public List<Benchmark> getAllBenchmarks() {
        return benchmarkRepository.findAll();
    }
}
