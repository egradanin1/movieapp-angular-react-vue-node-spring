package movieapp.project.movieapp.Repositories;

import java.util.List;
import java.util.UUID;
import movieapp.project.movieapp.Domain.Models.Movie;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface MovieRepository extends MongoRepository<Movie, String> {

    public Movie findByTitle(String title);
    public List<Movie> findAll();
    
}
