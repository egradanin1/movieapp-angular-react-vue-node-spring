package movieapp.project.movieapp.Repositories;

import java.util.List;
import java.util.UUID;
import movieapp.project.movieapp.Domain.Models.ChatMessage;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface ChatMessageRepository extends MongoRepository<ChatMessage, String> {

    public List<ChatMessage> findAll();
  
}
