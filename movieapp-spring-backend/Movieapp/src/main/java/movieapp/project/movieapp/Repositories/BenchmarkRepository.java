package movieapp.project.movieapp.Repositories;

import java.util.List;
import java.util.UUID;
import movieapp.project.movieapp.Domain.Models.Benchmark;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface BenchmarkRepository extends MongoRepository<Benchmark, String> {

    public List<Benchmark> findAll();
  
}
