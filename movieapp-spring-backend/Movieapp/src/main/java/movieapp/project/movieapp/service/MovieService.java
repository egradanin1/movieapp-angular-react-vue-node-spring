package movieapp.project.movieapp.service;

import movieapp.project.movieapp.Domain.Models.Movie;
import java.util.List;

public interface MovieService {
    void save(Movie movie);

    Movie findByTitle(String title);
    List<Movie> getAllMovies();
}
