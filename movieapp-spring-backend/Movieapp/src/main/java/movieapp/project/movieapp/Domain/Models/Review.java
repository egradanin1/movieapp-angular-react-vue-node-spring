package movieapp.project.movieapp.Domain.Models;

public class Review{


    private String title;
    private String username;
    private String comment;
    private Integer rating;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public Integer getRating() {
        return rating;
    }

    public void setRating(Integer rating) {
        this.rating = rating;
    }

	public Review(String title, String username, String comment, Integer rating) {
	    this.title = title;
	    this.username = username;
	    this.comment = comment;
        this.rating = rating;
	}

	public Review () {}
}
