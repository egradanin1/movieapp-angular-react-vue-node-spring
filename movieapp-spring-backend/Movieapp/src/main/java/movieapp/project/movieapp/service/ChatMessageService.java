package movieapp.project.movieapp.service;

import movieapp.project.movieapp.Domain.Models.ChatMessage;
import java.util.List;

public interface ChatMessageService {
    void save(ChatMessage chatMessage);
    List<ChatMessage> getAllChatMessages();
}
