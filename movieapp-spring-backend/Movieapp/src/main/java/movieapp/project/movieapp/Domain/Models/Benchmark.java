package movieapp.project.movieapp.Domain.Models;

import java.util.UUID;
import javax.persistence.*;
import java.util.Set;
import java.time.LocalDateTime;
import org.springframework.data.annotation.Id;



public class Benchmark extends BaseModel{


    private String action;
    private String duration;
    private String framework;

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public String getFramework() {
        return framework;
    }

    public void setFramework(String framework) {
        this.framework = framework;
    }

	public Benchmark(String action, String duration, String framework) {
	    this.action = action;
	    this.duration = duration;
	    this.framework = framework;
	}
    public Benchmark () {}
}
