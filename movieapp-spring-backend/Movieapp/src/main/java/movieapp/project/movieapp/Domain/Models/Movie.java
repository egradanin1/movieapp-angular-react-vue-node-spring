package movieapp.project.movieapp.Domain.Models;

import java.util.UUID;
import javax.persistence.*;
import java.util.Set;
import java.time.LocalDateTime;
import org.springframework.data.annotation.Id;
import java.util.List;
import java.util.ArrayList;
import movieapp.project.movieapp.Domain.Models.Review;


public class Movie extends BaseModel{


    private String title;
    private String description;
    private String image;
    private String video;
    private String publishedYear;
    private List<Review> reviews;
    private List<String> users;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getVideo() {
        return video;
    }

    public void setVideo(String video) {
        this.video = video;
    }
    public String getPublishedYear() {
        return publishedYear;
    }

    public void setPublishedYear(String publishedYear) {
        this.publishedYear = publishedYear;
    }

    public List<Review> getReviews() {
        return reviews;
    }

    public void setReviews(List<Review> reviews) {
        this.reviews = reviews;
    }


    public List<String> getUsers() {
        return users;
    }

    public void setUsers(List<String> users) {
        this.users = users;
    }


	public Movie(String title, String description, String image, String video , String publishedYear) {
	    this.title = title;
	    this.description = description;
	    this.image = image;
	    this.video = video;
	    this.publishedYear = publishedYear;
        this.reviews = new ArrayList<Review>();
        this.users = new ArrayList<String>();
	}
}
